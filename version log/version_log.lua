local process1 = io.popen("git_count.bat")
local revision = process1:read("*all")
process1:close()

local header = [[
<html>
	<head>
		<title>Update Summary</title>
		<link href="style.css" rel="stylesheet">
		<script src="formatter.js"></script>
	</head>
	<body>
		<div class="header">Commit History</div>
		<div class="revision">Revision: ]]..revision..[[</div>
		<div class="commit_history">]] 
local footer = [[
		</div>
	</body>
</html>]]

local html = header

function string.trim(s)
	s = s:gsub("[\"]", "")
	s = s:gsub("^%s*", "")
	return (s:gsub("^%s*(.-)%s*$", "%1"))
end
function string.split( s, separator )
	local t = {}
	if string.find(s, separator) then
		for token in string.gmatch( s, "[^" .. separator .. "]+" ) do
			table.insert( t, string.trim(token) )
		end
	else
		return {s}
	end
	return t
end
function parseTags( tags )
	local t = {}
	
	local raw = string.split(tags, ";")
	
	for k,v in pairs(raw) do
		if string.find(v,"=") then
			local variable = string.split(v,"=")
			t[variable[1]] = variable[2]
		else
			t[v] = true
		end
	end
	return t
end
function PrintTable(t)
	for k,v in pairs(t) do
		print("\t[ "..k.." ] => "..tostring(v))
	end
end

local process = io.popen("git_log.bat")
local output = process:read("*all")
process:close()
--print(output)

local commits = string.split( output, "~" )

local commit = nil
local c = nil
local body = nil
local tags = nil
for k,v in pairs(commits)do
	commit = {}
	c = string.split(v, "@")
	body = string.split(c[4], "#")
	if body[2] then
		tags = parseTags(body[2])
	else
		tags = {}
	end
	
	commit.timestamp = c[1]
	commit.username = c[2]
	commit.hash = c[3]
	commit.body = body[1]
	commit.tags = tags
	
	commits[k] = commit
end

function toHTML(s)
	return s:gsub("[\n]","<br/>")
end

for k,v in pairs(commits)do
	local row = [[
		
			<div class="commit-row">
				<ul class="tags-line">
]]
			
	for k,v in pairs(v.tags) do
		if type(v) == "boolean" then
			row = row .. [[					<li class="tag ]]..k.."\">"..k.."</li>\n"
		else
			row = row .. [[					<li class="tag ]]..k.."\">"..v.."</li>\n"
		end
	end
	
	row = row ..[[				</ul>
				<div class="meta">
					<div class="timestamp">]]..v.timestamp..[[</div>
					<div class="spacer"></div>
					<div class="hash">commit <a href=]].."\"https://bitbucket.org/kamilkz/car-company-tycoon/commits/"..v.hash.."\">"..v.hash..[[</a></div>
					<div class="spacer"></div>
					<div class="username">by <a href=]].."\"https://bitbucket.org/"..v.username.."\">"..v.username..[[</a></div>
				</div>
				<div class="summary">]]..toHTML(v.body)..[[
				</div>
			</div>]]
	html = html .. row
end

html = html .. footer
local file = io.open("output.html", "w")
file:write(html)
file:close()
print(html)
	
	
	