game.profile = {};

/**
Does the job.
Saves and loads JSON save games, basically
*/

(function(){
	var e = game.profile;
	
	e.playTime = 0;
	e.lastSave = 0;
	
	e.difficulty = 0;
	
	e.save = function(slot,autosave){
		var save = {};
		save.profile = $.extend(true, {}, this);
		save.company = $.extend(true, {}, game.company);
		save.tasks = $.extend(true, {}, game.tasks);
		save.date = $.extend(true, {}, game.date);
		
		save.toJSON = function() { 
			var attrs = {};
			for (var attr in this) {
				if (typeof this[attr] != "function") {
					attrs[attr] = this[attr]; 
				}
			}
			return attrs;
		};
		var saveJSON = JSON.stringify(save);
		
		localStorage.setItem("slot_"+slot, saveJSON);
		
		game.ui.updateSaveSlot(slot);
		if(game.debug==1){console.log(saveJSON);}
	};

	e.load = function(slot){
		var save = JSON.parse(localStorage.getItem("slot_"+slot));
		$.extend(true, this, save.profile);
		$.extend(true, game.date, save.date);
		$.extend(true, game.company, save.company);
		$.extend(true, game.tasks, save.tasks);
		
		if(game.debug==1){console.log(save);}
	};
	
	e.getMeta = function(slot){
		var save = JSON.parse(localStorage.getItem("slot_"+slot));
		
		return {lastSave: save.profile.lastSave, 
				playTime: save.profile.playTime, 
				difficulty: save.profile.difficulty, 
				companyName: save.company.name,
				playerName: save.company.playerName,
				money: save.company.money,
				date: save.date};
	};
	
	e.exists = function(slot){
		var save = localStorage.getItem("slot_"+slot);
		if (save===null) { return false; } else { return true; }
	};
	
	e.saveOptions = function(){
		var save = {};
		save.volume = $.extend(true, {}, game.music.volume);
		
		save.toJSON = function() { 
			var attrs = {};
			for (var attr in this) {
				if (typeof this[attr] != "function") {
					attrs[attr] = this[attr]; 
				}
			}
			return attrs;
		};
		var saveJSON = JSON.stringify(save);
		
		localStorage.setItem("options", saveJSON);
		
		if(game.debug==1){console.log(saveJSON);}
	};
	
	e.loadOptions = function(){
		var save = JSON.parse(localStorage.getItem("options"));
		if(save == undefined){ return; }
		
		if(save.volume != undefined){
			$.extend(true, game.music.volume, save.volume);
			
			//update sliders and text
			$( "#vol-slider" ).slider( "value", game.music.volume.volM*100 );
			$("#voval").html(game.music.volume.volM*100);
			
			$( "#vol-slider2" ).slider( "value", game.music.volume.volS*100 );
			$("#voval2").html(game.music.volume.volS*100);
			
			if(game.debug==1){console.log(save);}
		}
	};
})();