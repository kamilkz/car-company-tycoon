game.music = {};

//createjs.Sound.registerPlugins([createjs.WebAudioPlugin]);
createjs.Sound.alternateExtensions = ["mp3"];    // if the passed extension is not supported, try this extension
createjs.Sound.setVolume(1);
game.music.mid = 0;
game.music.playing = false;
game.music.bgm;
game.music.sfx;

sQueue = new createjs.LoadQueue(true);
sQueue.addEventListener("progress", handleProgress);
sQueue.addEventListener("fileload", handleLoad);
sQueue.installPlugin(createjs.Sound);
var base = "./sounds/";
game.music.tracks = [
	{src:base+"music/loop.ogg", title:"Menu", artist:"lantaren"},
	{src:base+"music/00 space.ogg", title:"space", artist:"lantaren"},
	{src:base+"music/01 suspense.ogg", title:"suspense", artist:"lantaren"},
	{src:base+"music/02 ambience.ogg", title:"ambience", artist:"lantaren"},
	{src:base+"music/03 mAmb dr.ogg", title: "more ambience", artist:"lantaren"}
];

game.music.sounds = [
	{src:base+"sfx/blip.ogg", id:"blip", data:"100"},
	{src:base+"sfx/blip up.ogg", id:"blip up", data:"100"},
	{src:base+"sfx/blip down.ogg", id:"blip down", data:"100"}
];

function handleLoad(event) { /* createjs.Sound.registerSound(event.item.src) */; console.log(event.item.src); if(event.item.src==game.music.tracks[0].src) { game.music.play(0); } }
function handleProgress(event) { game.ui.updateProgressBar("load", event.progress*100); }
game.music.playSound = function(id) { game.music.sfx = createjs.Sound.play(id); game.music.volume.setS(game.music.volume.volS);}

game.music.init = function() {
	game.music.bgm = createjs.Sound.play("");
	sQueue.loadManifest(game.music.sounds);
	sQueue.loadManifest(game.music.tracks);
	sQueue.load();
	
}

game.music.p = function(id, s) {
	if (id=='pause'||id=='ps') { game.music.pause(); }
	else if (id=='prev'||id=='pr') { game.music.prev(); }
	else if (id=='next'||id=='nx') { game.music.next(); }
	else if (s==true) { game.music.playSound(id); }
	else { }
}

game.music.next = function() { game.music.bgm.stop(); if( game.music.mid>=game.music.tracks.length-1 ) { game.music.mid=0; } else { game.music.mid++; } game.music.play(game.music.mid); };
game.music.prev = function() { game.music.bgm.stop(); if( game.music.mid<=0 ) { game.music.mid=game.music.tracks.length-1; } else { game.music.mid--; } game.music.play(game.music.mid); };
game.music.pause = function() { if (game.music.playing) {game.music.playing=false; game.music.bgm.pause();} else {game.music.playing=true; game.music.bgm.resume();} };
game.music.play = function(i) {
	game.music.playing=true;
	game.music.bgm = createjs.Sound.play(game.music.tracks[i].src);
	game.music.volume.setM(game.music.volume.volM);
	game.music.bgm.addEventListener("complete", game.music.next);
	game.music.calculate();
};

game.music.calculate = function() {
	if(game.music.playing) {
		length = Math.round(game.music.bgm.getDuration() / 1000);
		cur = Math.round(game.music.bgm.getPosition() / 1000);
		$("#sound-control" ).dialog("option", "title", game.music.tracks[game.music.mid].artist +" - "+ game.music.tracks[game.music.mid].title + " | " + sec2mins(cur) + " / " + sec2mins(length));
	}
}
function sec2mins(seconds) {
	min = Math.floor(seconds / 60);
	sec = seconds % 60;
	return min + ":" + ( sec<10 ? "0" + sec : sec);
}

game.music.volume = {
	setM : function(v){game.music.bgm.volume = v; this.volM = v;},
	getM : function(){ return game.music.bgm.volume; },
	volM : 0.3,
	setS : function(v){game.music.sfx.volume = v; this.volS = v;},
	getS : function(){ return game.music.sfx.volume; },
	volS : 0.3
}