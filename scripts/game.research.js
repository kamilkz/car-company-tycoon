game.research = {};

/**
Have to make the UI for this, and think of some messages to show when they're done.
Also needs the durations changed to weeks from seconds.
Then later it will surely need a hell of a lot of balancing
*/

(function(){
	var e = game.research;
	
	var n = "General"; //readable-type
	var t = "general"; //engine-type
	
	e.ResearchCarType = {
		id : "new_car_type",
		name : "New Car Type",
		pointsCost : 10,
		duration : 5, //5 weeks? too much? decimals work too..
		category : t,
		categoryName : n,
		infinite : true
	};
	e.CustomChassis = {
		id : "custom_chassis",
		name : "Custom Chassis",
		pointsCost : 50,
		duration : 5,
		category : t,
		categoryName : n
	};
	
	e.Testing = {
		id : "testing",
		name : "Test",
		pointsCost : 50,
		duration : 3,
		category : t,
		categoryName : n
	};

	e.GeneralItems = [e.Testing, e.ResearchCarType, e.CustomChassis];
	
	n = "Engine";
	t = "engine";
	/*
	e.SmallEngine = {
		name : "Small Engine",
		pointsCost : 10,
		manfCost : 2e3,
		duration : 1e4,
		techLevel : 0,
		canResearch : function (e) {
			return 2 < ghg24.ghg28(e, "2D Graphics V3")
		},
		category : t,
		categoryName : n,
		group: "engine"
	}
	*/
	
	e.SmallEngine = {
		id : "small_engine",
		name : "Small Engine",
		manfCost : 1e3,
		techLevel : 0,
		category : t,
		categoryName : n,
		group: "engine"
	};
	e.MediumEngine = {
		id : "medium_engine",
		name : "Bigger Engine",
		pointsCost : 10,
		manfCost : 25e2,
		cost : 5e3,
		duration : 1e5,
		techLevel : 2,
		canResearch : function () {
			return true
		},
		category : t,
		categoryName : n,
		group: "engine"
	};
	e.BigEngine = {
		id : "big_engine",
		name : "Powerful Engine",
		pointsCost : 25,
		manfCost : 5e3,
		cost : 1e4,
		duration : 1e5,
		techLevel : 4,
		targetDivision: "engine",
		canResearch : function () {
			return game.company.experience.getLevel("engine") > 4
		},
		category : t,
		categoryName : n,
		group: "engine"
	};
	e.SportEngine = {
		id : "sport_engine",
		name : "Race Engine",
		pointsCost : 50,
		manfCost : 15e3,
		cost : 25e3,
		duration : 2e5,
		techLevel : 6,
		targetDivision: "engine",
		canResearch : function () {
			return game.company.experience.getLevel("engine") > 9
		},
		category : t,
		categoryName : n,
		group: "engine"
	};
	
	e.EngineItems = [e.SmallEngine, e.MediumEngine, e.BigEngine, e.SportEngine];
	
	n = "Fuel Economy";
	t = "fuel economy";
	e.Efficiency1 = {
		id : "fuel_economy_1",
		name : "Air/Fuel Ratio Adjustment",
		pointsCost : 5,
		cost : 1e3,
		manfCost : 5e2,
		duration : 1e4,
		techLevel : 1,
		canResearch : function () {
			return game.company.experience.getLevel("engine") > 1
		},
		category : t,
		categoryName : n
	};
	e.Efficiency2 = {
		id : "fuel_economy_2",
		name : "Better Combustion Design",
		pointsCost : 10,
		cost : 25e2,
		manfCost : 5e2,
		duration : 2e4,
		techLevel : 2,
		canResearch : function () {
			return game.company.experience.getLevel("engine") > 3
		},
		category : t,
		categoryName : n
	};
	e.Efficiency3 = {
		id : "fuel_economy_3",
		name : "Easier flow",
		pointsCost : 25,
		cost : 1e4,
		manfCost : 1e3,
		duration : 3e4,
		techLevel : 2,
		targetDivision: "engine",
		canResearch : function () {
			return game.company.experience.getLevel("engine") > 7
		},
		category : t,
		categoryName : n
	};
	
	e.FuelEconomyItems = [e.Efficiency1, e.Efficiency2, e.Efficiency3];
	
	n = "Car Size";
	t = "car size";
	
	//something that increases quality standards and sales.
	//no idea what that would be.
	
	e.KitCar = {
		id : "project_size_1",
		name : "Kit-Car",
		pointsCost : 0,
		manfCost : 5e3,
		category : t,
		categoryName : n
	};
	e.HomeMadeCar = {
		id : "project_size_2",
		name : "Home-Made Cars",
		pointsCost : 50,
		cost : 5e4,
		manfCost : 4e4,
		duration : 1e4,
		canResearch : function () {
			return game.company.cars.length > 4 && game.company.getLevel() > 1 //in bigger garage + made atleast 5 cars
		},
		category : t,
		categoryName : n
	};
	e.ProductionCar = {
		id : "project_size_3",
		name : "Production Cars",
		pointsCost : 150,
		cost : 1e6,
		manfCost : 4e5,
		duration : 1e4,
		canResearch : function () {
			return game.company.cars.length > 24 && game.company.getLevel() > 2 && game.company.hasBuilding("design studio") //in bigger+better garage + has design studo + made atleast 25 cars
		},
		category : t,
		categoryName : n
	};
	
	n = "Car Design";
	t = "car design";
	
	e.CarSize = {//unlocks medium and large; before this body/style has no effect on size
		id : "car_sizes",
		name : "Car Sizes",
		pointsCost : 25,
		cost : 15e3,
		duration : 2e4,
		canResearch : function () {
			return game.company.cars.length > 4 && game.company.getLevel() > 1 //in bigger garage + made atleast 5 cars
		},
		category : t,
		categoryName : n
	};
	e.Generations = { //"sequels"
		id : "generations",
		name : "Generations",
		pointsCost : 20,
		duration : 13e3,
		cost : 8e4,
		manfCost : 2e4,
		canResearch : function (e) {
			return game.isLaterOrEqualThan(8, 6)
		},
		category : t,
		categoryDisplayName : n
	};
		
	
	e.SpecialItems = [e.KitCar, e.HomeMadeCar, e.ProductionCar, e.CarSize, e.Generations];
	
	e.startResearch = [e.KitCar, e.SmallEngine];
	
	e.getAllItems = function () {
		return e.GeneralItems.concat(e.EngineItems).concat(e.FuelEconomyItems).concat(e.SpecialItems);
	};
	
	e.getItemByID = function (id) {
		for (var r = this.getAllItems(), i = 0; i < r.length; i++){
			if (r[i].id === id){
				return r[i];
			}
		}
	};
	
	e.checkNewResearch = function () {
		for (var newResearch = [], n = game.company, r = this.getAllItems().except(e.startResearch.concat(n.researchCompleted.concat(n.researchAvailable))), i = 0; i < r.length; i++) { //all non-researd and already available researches
			var s = r[i];
			if(s.canResearch && s.canResearch(n) || !s.canResearch){ //canResearch or not conditional
				if(s.targetDivision && n.hasBuilding(s.targetDivision) || !s.targetDivision){ //is a division research, and division is there?
					if(s.techLevel && n.canDevelopChassis() || !s.techLevel){ //is engine part and engines are unlocked, otherwise yes
						if(-1 == newResearch.indexOf(s)){ //not already there
							newResearch.push(s);
						}
					}
				}
			}
		}
		if (0 < newResearch.length) {
			r = "New research available:";
			for (i = 0; i < newResearch.length; i++)
				s = newResearch[i], n.researchAvailable.push(s), r += "\n<br/>" + s.name;
			newResearch = {header:"New Research!", text:r};
			game.notifications.push(newResearch);
		}
	};
	
	e.canResearch = function( r ) {
		if(r.pointsCost != undefined && game.company.getResearchPoints()>=r.pointsCost || r.pointsCost == undefined){
			if(r.canResearch != undefined && r.canResearch(game.company) || r.canResearch == undefined){ //canResearch or not conditional
				if(r.targetDivision != undefined && game.company.hasBuilding(r.targetDivision) || r.targetDivision == undefined){ //is a division research, and division is there?
					if(r.techLevel != undefined && game.company.canDevelopChassis() || r.techLevel == undefined){ //is engine part and engines are unlocked, otherwise yes
						return true;
					}
				}
			}
		}
		return false;
	};
	
	e.isComplete = function( task ) {
		var weekDecimal = game.date.getWeekDecimal();
		var research = task.research;
		
		if((weekDecimal-task.startWeek)>=research.duration){
			var researchItem = {
				"id" : research.id,
				"startWeek" : task.startWeek,
				"finishWeek" : weekDecimal,
				"complete" : true
			};
			
			if(research.onCompleted != null){
				research.onCompleted( researchItem );
			}
			
			if(research.infinite == null || research.infinite == false){
				game.company.researchCompleted.push(researchItem);
				game.company.researchAvailable.removeItems(research);
			}
			
			game.notifications.researchCompleted(research);
			
			this.checkNewResearch();
			
			return true;
		}else{
			return false;
		}
	};
})();