game.company = {};

(function(){
	var e = game.company;
	
	e.playerName = "Player Name";
	e.name = "Company Name";
	e.money = 5000;
	
	e.setName = function( name ){ this.name = name; };
	e.getName = function() { return this.name; };
	
	e.getMoney = function() { return this.money; };
	e.setMoney = function(money) { this.money = money; };
	e.adjustMoney = function(delta) { this.money += delta; };
	
	e.experience = {
		getExpToNextLevel : function( topic ) { return game.experienceLevels[this.ratings[topic].level+1] - this.ratings[topic].experience; },
		getLevel : function( topic ){ return this.ratings[topic].level; },
		getStat : function( topic ){ return this.ratings[topic].experience; },
		setStat : function( topic, exp ) { this.ratings[topic].experience = exp; },
		adjustStat : function( topic, delta ) { this.ratings[topic].experience += delta; this.update(topic);},
		update : function( topic ) { while(game.experienceLevels[this.ratings[topic].level+1]<= this.ratings[topic].experience){this.ratings[topic].level+=1;} },
		ratings : {
			"company":{ "level":0, "experience":0 },
			"engine":{ "level":0, "experience":0 },
			"styling":{ "level":0, "experience":0 }
			//just inits all the ratings, put some more if you can think of good ones
		}
	};
	
	e.contractsAvailable = [{
			name : "Change oil",
			task : "oil",
			time : 3e4,
			reward : 5e2, //$500 for an oil change, YEP.
			penalty : 5e1
		}];
		
	e.contractsCompleted = [{
			name : "Fix Mr.Joe's car",
			task : "repair",
			extended : false, //extended is where you still control what to do like a "contract" instead of "job"
			allowedTime : 3e4, //30 seconds, we need a time calculator for the dates.
			completed : true,
			startTime : 23141,
			endTime : 12314
		}];
		
	e.lastCar = {};
	e.cars = [];
	/*{
			name : "Finished example",
			id : "uuid",
			startWeek : 1.231451, //yep.
			finishWeek : 6.231451,
			all the variables, 
			car type, etc
			hype points,
			sales log,
			features and weights
			scores
		}, {
			cba for this.
		}];*/
	
	//cars, training, contracts in progress put here:
	e.currentTasks = [];
		/*{
			task : 
			divison/empolyee :
			startTime :
			endTime/progress : 
		}*/
	
	e.employees = [{
			name : "The player",
			division : "all",
			expertise : "engine",
			carsCreated : 51.134,
			
			timeStarted : 0,
			empolyeeTime : 12314, //how long they were there
			currentlyHired : true,
			
			techPoints : 893,
			designPoints : 1241,
			experience : 12141,
			level : 11
		}];
			
	
	e.divisions = [{
			id : "garage",
			building : "Garage",
			//something else here?
		}];
		
	e.hasBuilding = function( building ){
		for(var i=0;i<this.divisons.length;i++){
			var b = this.divisons[i].id;
			if(b==building){ return true; }
		}
		return false;
	};
	
	//level is the building level, not a rank/xp
	e.level = 1;
	e.getLevel = function() {
		return this.level;
	};
	
	
	e.researchPoints = 0;
	e.getResearchPoints = function(){ return this.researchPoints; };
	e.setResearchPoints = function(p) { this.researchPoints = p; };
	e.adjustResearchPoints = function(d) { this.researchPoints += d; };
	
	e.researchedChassis = false;
	e.researchCompleted = [];
	e.researchAvailable = [];
	
	e.canDevelopChassis = function(){
		return this.researchedChassis;
	};
	
	e.completeCar = function(car){
		var score = game.carcalc.getSub(car,1);
		var t = "Score: "+score+
				"\nCategory: "+car.category+
				"\nStyle: "+car.style+
				"\nSize: "+car.size+
				"\nPerformance: "+car.variables[0]+
				"\nStyling: "+car.variables[1]+
				"\nComfort "+car.variables[2];
		game.notifications.push({header:"Car Complete",text:car.name+"  "+t});
	};
	
	e.init = function(){
		this.researchCompleted = game.research.startResearch;
		this.researchAvailable = game.research.GeneralItems;
	};
	
})();