game.carcalc = {};

/**
This should be more complex, and have a "gameplay" aspect to it; not just be based on making cars on values
For example, in GDT you have to be consistent to get good ratings, but good ratings dont even mean good sales
*/

(function(){
	var e = game.carcalc;
	
	var vFalloff = 0.25; //variable difference falloff (steepness; exponential)
	var pFalloff = 1; //price difference falloff (steepness; linear
	var pRank = ['2.40','2.45','2.50','2.55','2.60','2.65','2.70','2.75','2.80','2.85','2.90','2.95','3.00'];
	var expMultiplier = 4; //experience multiplier; the more you get into the game, the lower it gets (smallest # possible to maintain a 10-max score is 1)
	
	//needs trend matching!
	//maybe other things, i probably forgot half.
	e.getSub = function( car , dbg){
	
		switch(game.profile.difficulty){
			case 0: expMultiplier=4; break;
			case 1: expMultiplier=6; break;
			case 2: expMultiplier=4; break;
			case 3: expMultiplier=2; break;
			case 4: expMultiplier=1; break;
		}
			
		var carVars				= car.variables;
		var tv					= carVars[0] + carVars[1] + carVars[2];
		var perfectVars 	    = game.carcategories[car.category].multipliers;
		
		if(carVars[0]==0&&carVars[1]==0&&carVars[2]==0) {tv=3; carVars=[1,1,1];}
		
		var carRatio			= [(carVars[0]/tv).toFixed(2),(carVars[1]/tv).toFixed(2),(carVars[2]/tv).toFixed(2)];
		
		var ttlMult				= game.math.variableSum(perfectVars); //
		var carMult				= [perfectVars[0]/ttlMult, perfectVars[1]/ttlMult, perfectVars[2]/ttlMult] //am, bm, cm
		
		var carStyle 			= game.carstyles.get(car.style);
		var typeMultiplier 		= carStyle.typeMultiplier[game.carcategories.getNumber(car.category)];
		var sizeMultiplier 		= carStyle.sizeMultiplier[car.size];
		
		var diff1 = Math.abs(carRatio[0]-carMult[0]); //ta
		var diff2 = Math.abs(carRatio[1]-carMult[1]); //tb
		var diff3 = Math.abs(carRatio[2]-carMult[2]); //tc
		
		var variableScore = Math.pow(diff1*vFalloff, diff1*vFalloff)+Math.pow(diff2*vFalloff, diff2*vFalloff)+Math.pow(diff3*vFalloff, diff3*vFalloff); //dm
		var vsQ = (Math.round(variableScore * 20) / 20).toFixed(2).toString();
		var vsR = pRank.indexOf(vsQ)/2;
		var subScore = (Math.round((((vsR * 3) + typeMultiplier + sizeMultiplier + expMultiplier)/3-0.17) * 2) / 2); //3+1+1+1
		
		if (subScore>10){subScore=10;}
		
		if(dbg==1){ //if debug enabled
			logStatsClr();
			logStatsB("performance: "+carRatio[0]+'%');
			logStatsB(" | styling: "+carRatio[1]+'%');
			logStatsB(" | comfort: "+carRatio[2]+'%');
			logStats("Performance R: "+carMult[0].toFixed(2));
			logStats("Styling Ratio: "+carMult[1].toFixed(2));
			logStats("Comfort Ratio: "+carMult[2].toFixed(2));
			logStats(variableScore.toFixed(2)+'|'+vsQ);
			logStatsB("--"+subScore.toFixed(2));
			logStats(typeMultiplier+" | "+sizeMultiplier);
			logStats("p-rank: "+vsR);
			logStats("exp-mult: "+expMultiplier);
		}
		
		return subScore;
	}
	
	e.dbg = function() {
		var cat = $("#cSel").val();
		var size = $("#sSel").val();
		var style = $("#tSel").val();
		
		var car = {
			category : cat,
			style : style,
			size : size,
			variables : [$("#a-slider").slider('value'), $("#b-slider").slider('value'), $("#c-slider").slider('value')]
		};
		e.getSub(car, 1);
	}
})();