/**
This doesn't really do much more than this, I'd say it's done
Maybe just balancing in the future
*/
game.carcategories = {
	"sports":{
		name : "Sports Car",
		multipliers : [100, 75, 50]
	}, "compact": {
		name : "Compact",
		multipliers : [50, 75, 50]
	}, "subcompact": {
		name : "City Car",
		multipliers : [25, 75, 50]
	}, "family": {
		name : "Family Car",
		multipliers : [50, 50, 100]
	}, "luxury": {
		name : "Luxury",
		multipliers : [75, 75, 100]
	}, "supercar": {
		name : "Super Car",
		multipliers : [100, 75, 50]
	}, "offroader": {
		name : "Off-roader",
		multipliers : [100, 25, 25]
	}, "utility": {
		name : "Utility",
		multipliers : [75, 25, 50]
	}, getNumber: function(name){
		switch(name){
			case "sports": 		return 0; break;
			case "compact": 	return 1; break;
			case "subcompact": 	return 2; break;
			case "family": 		return 3; break;
			case "luxury": 		return 4; break;
			case "supercar": 	return 5; break;
			case "offroader": 	return 6; break;
			case "utility": 	return 7; break;
			default: return 0; break;
		}
	}
};