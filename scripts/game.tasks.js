game.tasks = {};

/**
This turned out to be a pain in the ass, but it works
Will need lots of refactoring
And we just add elements as we go along: research, car making, contracts.
*/

(function(){
	var e = game.tasks;
	var c = game.company;
	
	e.currentTasks = [];
	
	e.isResearching = false;
	e.developingCar = false;
	e.devCarID = 0;
	e.createNewCar = function(){
		var currentDate = game.date.getWeekDecimal();
		var newCarTask = {
			type : "car",
			//id : guid(),
			startWeek : currentDate,
			stage : 0, //integer
			stageProgress : 0, //decimal
			stageStart : currentDate,
			started : true,
			assignee : "The player",//ugh..
			
			//putting all the variables into the task too, it's pointless making a car and a carTask
			name : "Example",
			category : "sports",
			style : "hatchback",
			size : 1,
			variables : [50,50,50]
		};
		
		this.currentTasks.push(newCarTask);
		this.developingCar = true;
		
		return this.currentTasks[this.currentTasks.length-1]; //return >reference< to the task
	};
	
	e.startResearch = function(research){//researchID){
		//var research = game.research.getItemByID(researchID);
		//if (research==null){ return; } //no research found, dont do anything; simplezz
		if(research.pointsCost != undefined){ c.adjustResearchPoints(-research.pointsCost); }
		if(research.cost != undefined){ c.adjustMoney(-research.cost); }
	
		var currentDate = game.date.getWeekDecimal();
		var researchTask = {
			type : "research",
			//id : guid(),
			startWeek : currentDate,
			assignee : "The player",
			research : research
		};
		
		this.currentTasks.push(researchTask);
		this.isResearching = true;
	}
	
	e.getDevCar = function(){
		return this.currentTasks[this.devCarID];
	};
	
	/*car development stages: design, prototype, safety testing, quality assurance, manfuacturing
	marketing can be applied at any stage, but it's best done after the design phase
	in the design phase, the car is designed (setting variables), iterations can be created at any time but if there is a hype(or whatever) 
		for a car that is scrapped, a penalty is applied, maybe a loss of fans or sales
	in design phase, the most important is getting the values correct, but it also increases overall costs depending on what you pick
	in the prototype a budget is assigned which will improve the overall score and design, beter workers and facilities mean less time 
		is spent on this stage, money wise though, it will will probably cost a lot
	in the safety testing phase, a safety rating is given, the player could then refine safety if it's not so good, or just leave it
	in the quality assurance, a budget and time limit is given, a higher budget and a longer time mean better ratings because the car has been reworked
		a short time limit with high budget wont do anything because there is a "warm-up"
		a long time limit with a low budget will not do much, but would so something
		a medium time limit with a medium budget will help get the ratings up
		but the best is to have a medium/long time limit with a high budget, this will get ratings up a lot
	in the manufacturing stage, an option can be chosen to manufacture x amount of cars
		after the development, more cars need to be made, a sales chart will show production+stock vs. demand

	alternative:
	only 3 stages: design, prototype, manufacture
	the design phase is exactly the same
	but the prototype phase has a time limit and budget, but also sliders for eg. "emphasis on safety" or "emphasis on ergonomics"
		possible sliders:
			maintenance costs
			ergonomics
			safety
		this would work out quite well with the car types/genre, a workhorse pickup would surely need lower maintenance than ergonomics, 
			while a luxury car should have ergonomics as the top priority
	the manufacturing phase would be the same as well

	also, in the design phase you should be able to chose whether the car is a concept car or production car
	*/
	e.checkCompletedTasks = function(){
		var weekDecimal = game.date.getWeekDecimal();
		for (r = this.currentTasks, i = 0; i < r.length; i++) {
			var t = r[i];
			if(t.type=="car"){
				var timeAlloc = t.variables[t.stage]/100;
				var timePerStage = timeAlloc * 4; //1.0 (100) = 4 weeks, 0.5 (50) = 2 weeks, easily changeable, needs to be better though.
				if ((t.stageStart+timePerStage)<=weekDecimal){
					t.stage+=1;
					if(t.stage==3){
						this.currentTasks.splice(i, 1);
						
						var car = {
							name : t.name,
							//id : guid(),
							startWeek : t.startWeek,
							finishWeek : weekDecimal,
							complete : true,
							variables : t.variables,
							category : t.category,
							style : t.style,
							size : t.size
						};
						
						c.cars.push(car);
						var carO = c.cars[c.cars.length-1];
						c.completeCar(carO);
						this.developingCar = false;
						
						game.notifications.carCompleted(carO);
					
						game.research.checkNewResearch();
						
					}else{
						t.stageStart = weekDecimal;
						t.stageProgress = 0;
						game.ui.displayCarDevDialog(t, t.stage);
					}
				}else{
					t.stageProgress = (weekDecimal-t.stageStart)/timePerStage;
				}
				game.ui.updateProgressBar("car", t.stage+t.stageProgress, t.stage );
			}
			else if(t.type=="research"){
				if(game.research.isComplete(t.research)){
					this.currentTasks.splice(i, 1);
					
					game.ui.updateProgressBar("research");
					
					this.isResearching = false;
				}else{
					game.ui.updateProgressBar("research", (weekDecimal-t.startWeek)/t.research.duration);
				}
			}
		}
	};
	
	e.init = function(){
	};
	
})();