$(function() {
		function switchToMainMenu(){
			$("#gameArea").hide();
			$("#mainMenu").show();
			$("#main-menu").dialog( "open" );
			$("#pause-menu").dialog( "close" );
			game.inMainMenu = true;
			game.state = 2;
		}
		function switchToGame(){
			$("#gameArea").show();
			$("#mainMenu").hide();
			$("#main-menu").dialog( "close" );
			game.inMainMenu = false;
			game.state = 1;
		}

		$( "#car-progress" ).progressbar({max: 300});
		$( "#research-progress" ).progressbar();
		$( "#load-progress" ).progressbar();
		
		$( "#a-slider" ).slider({
			orientation: "vertical",
			range: "min",
			max: 100,
			value: 100,
			slide: function( event, ui ) {
				game.ui.handleSlider($(this).attr("id"),ui.value);
			}
		});
		
		$( "#b-slider" ).slider({
			orientation: "vertical",
			range: "min",
			max: 100,
			value: 100,
			slide: function( event, ui ) {
				game.ui.handleSlider($(this).attr("id"),ui.value);
			}
		});
		
		$( "#c-slider" ).slider({
			orientation: "vertical",
			range: "min",
			max: 100,
			value: 100,
			slide: function( event, ui ) {
				game.ui.handleSlider($(this).attr("id"),ui.value);
			}
		});
		
		
		$( "#date-slider" ).slider({
			orientation: "horizontal",
			range: "min",
			max: 100,
			min: 1,
			value: 1,
			slide: function( event, ui ) {
				$("#ds-val").html(ui.value);
				game.ui.handleDateSlider(ui);
			}
		});
		
		//setTimeout(function() { $( "#notifications-menu" ).hide( "puff", {percent:105}, 200 ); }, 1000);
		//setTimeout(function() { $( "#options-menu" ).show( "scale", {percent:100}, 200 ); }, 2000);
		/*******************************************
		EXAMPLE USAGE WHICH SHOULD BE IN THE GAME YO
		*******************************************/
		
		$( "#game-dialog-message" ).dialog({
			dialogClass: "no-close thin",
			autoOpen: false,
			width: 600,
			buttons: [ { text: "Ok", click: function() { $( this ).dialog( "close" ); } }],
			draggable: false,
			resizeable: false,
			modal: true,
			position: {my: "center center", at: "center center", collision:"fit"}
		});
			
		$( "#confirm-exit" ).dialog({
			autoOpen: false,
			width: 600,
			buttons: [
				{ text: "Yes", click: function() { switchToMainMenu(); $( this ).dialog( "close" ); } },
				{ text: "Cancel", click: function() { $( this ).dialog( "close" ); }} ], 
			draggable: false,
			resizeable: false,
		});
				
		$( "#debug-menu" ).dialog({
			dialogClass: "no-close thin invert",
			autoOpen: false,
			width: 400,
			height: 350,
			resizable: false,
			position: {my: "left bottom", at: "left bottom+20", collision:"fit"},
			draggable: true
		});
		
		$( "#console" ).dialog({
			dialogClass: "no-close thin invert",
			autoOpen: false,
			width: 400,
			height: 275,
			resizable: false,
			position: {my: "right top", at: "right center", collision:"fit"},
			draggable: true
		});
		
		$( "#sound-control" ).dialog({
			dialogClass: "no-close thin invert",
			autoOpen: true,
			width: 400,
			height: 75,
			resizable: false,
			position: {my: "right bottom", at: "right bottom+20", collision:"fit"},
			draggable: true
		});
			
		$( "#pause-menu" ).dialog({
			autoOpen: false,
			width: 300,
			height: 'auto',
			resizable: false,
			dialogClass: "no-close",
			modal: true,
			draggable: false
		});
		
		$( "#save-menu" ).dialog({
			autoOpen: false,
			width: 600,
			height: 'auto',
			resizable: false,
			dialogClass: "no-close",
			modal: false,
			draggable: false
		});
			
		$( "#sett-menu" ).dialog({
			autoOpen: false,
			width: 600,
			height: 'auto',
			resizable: false,
			draggable: false
		});
			
		$( "#design-car" ).dialog({
			autoOpen: false,
			width: 800,
			height: 600,
			resizable: false,
			modal: true,
			draggable: false
		});
		
		$( "#research-menu" ).dialog({
			autoOpen: false,
			width: 800,
			height: 600,
			resizable: false,
			modal: true,
			draggable: false,
			buttons: [
				{ text: "Research", click: function() { if (game.ui.startResearch()){ $( this ).dialog( "close" ); game.setState(1);} } },
				{ text: "Cancel", click: function() { $( this ).dialog( "close" ); game.setState(1); }} ]
		});
			
		$(function() { $( "#pmenu" ).menu(); });
		$(function() { $( "#smenu" ).menu(); });
		$(function() { $( "#svmenu" ).menu(); });
		//$("#tabs").tabs();
		$("#design-tabs").tabs();
		$( "#icons li, .menu-button" ).hover(
			function() { $( this ).addClass( "ui-state-hover" );},
			function() { $( this ).removeClass( "ui-state-hover" );} );
				
		//$( "#pause-btn, #icons li" ).hover(
		//	function() { $( this ).addClass( "ui-state-hover" ); },
		//	function() { $( this ).removeClass( "ui-state-hover" ); } );
		var consoleOpen = 0;
		function toggleConsole() { consoleOpen==0 ? consoleOpen=1 : consoleOpen = 0; }
		
		$( "#cheat-btn" ).click(function( event ) { game.company.adjustMoney(1000000); game.company.adjustResearchPoints(100000); event.preventDefault(); });
		$( "#create-car-btn" ).click(function( event ) { game.ui.createCar(); });
		$( "#research-btn" ).click(function( event ) { game.setState(2); $( "#research-menu" ).dialog( "open" ); game.ui.populateResearches(); });
		$( "#pause-btn" ).click(function( event ) { game.setState(2); $( "#pause-menu" ).dialog( "open" );  });
		$( "#settings-btn" ).click(function( event ) { game.setState(2); $( "#sett-menu" ).dialog( "open" ); });
		$( "#dbg" ).click(function( event ) { game.toggledbg(); $(this).html("Debug - "+game.debug); if(game.debug==1){$( "#debug-menu" ).dialog( "open" );} else {$( "#debug-menu" ).dialog( "close" ) }});
		$( "#con" ).click(function( event ) { toggleConsole(); $(this).html("Console - "+consoleOpen); (consoleOpen==1) ? $( "#console" ).dialog( "open" ) : $( "#console" ).dialog( "close" ); });
		$( "#pmenuX" ).click(function( event ) { toggleConsole(); game.setState(1); $( "#pause-menu" ).dialog( "close" );});
		$( "#quitbtn" ).click(function( event ) { $( "#confirm-exit" ).dialog( "open" ); });
		$( "#pause-menu" ).on("dialogclose", function( event ) { game.setState(1); });
		$( "#design-car" ).on("dialogclose", function( event ) { game.setState(1); });
		$( "#debug-menu" ).on("dialogclose", function( event ) { if(game.debug==1){ $( "#debug-menu" ).dialog( "open" );} });
		$( "#sound-control" ).on("dialogclose", function( event ) { $( "#sound-control" ).dialog( "open" );} );
		$( "#game-dialog-message" ).on("dialogclose", function( event ) { game.date.increment(); game.setState(1); game.notifications.dialogClosed(); });
		$( "#game-dialog-message" ).on("dialogopen", function( event ) { game.setState(2)} );
		//if there's many other possible ways to close a dialog, just use this ^ and let the buttons close it only, it's easier to manage that way.
			
		$( ".svgbtn" ).click(function( event ) { 
			$("#save-menu" ).dialog( "open" );
			if(game.inMainMenu){ $("#save-menu" ).dialog("option", "title", "Continue"); }
			else { $("#save-menu" ).dialog("option", "title", "Save"); }
		});
		
		function handleSaveSlot(number){
			if(game.inMainMenu){
				if(game.profile.exists(number)){
					game.profile.load(number);
					$( "#save-menu" ).dialog( "close" );
					$( "#main-menu" ).dialog( "close" );
					switchToGame();
					game.start();
				}
			}else{
				game.profile.save(number);
			}
		}
		$( "#sv1" ).click(function( event ) { handleSaveSlot(1); });
		$( "#sv2" ).click(function( event ) { handleSaveSlot(2); });
		$( "#sv3" ).click(function( event ) { handleSaveSlot(3); });
		$( "#sbk" ).click(function( event ) { $( "#save-menu" ).dialog( "close" ); });
		
		$( window ).resize(function() {
			$( "#game-dialog-message" ).dialog( "option", "position", { my: "center", at: "center", of: window });
			$( "#debug-menu" ).dialog( "option", "position", { my: "left bottom", at: "left bottom+20", of: window });
			$( "#sound-control" ).dialog( "option", "position", { my: "right bottom", at: "right bottom+20", of: window });
			$( "#console" ).dialog( "option", "position", { my: "right top", at: "right center", of: window });
			$( "#pause-menu" ).dialog( "option", "position", { my: "center", at: "center", of: window });
			$( "#design-car" ).dialog( "option", "position", { my: "center", at: "center", of: window });
			$( "#sett-menu" ).dialog( "option", "position", { my: "center", at: "center", of: window });
			$( "#main-menu" ).dialog( "option", "position", { my: "center", at: "center", of: window });
		});
		
		/*
		Main menu things:
		*/
		
		$( "#start-game" ).dialog({
			autoOpen: false,
			width: 600,
			draggable: false,
			resizable: false,
			buttons: [
				{ text: "Ok", click: function() { 
					game.saveSlot = 0;
					$( this ).dialog( "close" );
					$( "#main-menu" ).dialog( "close" );
					switchToGame()
					game.start();
					game.newProfile();
				} },
				{ text: "Cancel", click: function() { $( this ).dialog( "close" ); }}
			] });
		
		$( "#main-menu" ).dialog({
			autoOpen: true,
			width: 300,
			height: 'auto',
			resizable: false,
			dialogClass: "no-close",
			modal: false,
			draggable: false
		});
		
		/*$( "#cont-menu" ).dialog({
			autoOpen: false,
			width: 800,
			height: 'auto',
			resizable: false,
			modal: false
		});*/
		
		$( "#cred-menu" ).dialog({
			autoOpen: false,
			width: 800,
			height: 400,
			resizable: false,
			draggable: false,
			modal: true
		});
		
		$(function() { $( "#menu" ).menu(); });
		$(function() { $( ".smenu" ).menu(); });
		$(function() { $( "#cmenu" ).menu(); });
		$(function() { $( "#radio" ).buttonset(); });
		
		$( "#ngbtn" ).click(function( event ) { $( "#start-game" ).dialog( "open" );});
		$( ".sbtn" ).click(function( event ) { $( "#sett-menu" ).dialog( "open" );});
		$( "#cbtn" ).click(function( event ) { $( "#cred-menu" ).dialog( "open" );});
		
		$( "#cmbtn" ).click(function( event ) { $( "#cont-menu" ).dialog( "close" );});
		$( "#sback" ).click(function( event ) { $( "#sett-menu" ).dialog( "close" );});
		
		$("#tabs").tabs();
			
		$( "#vol-slider" ).slider({
			orientation: "horizontal",
			range: "min",
			max: 100,
			min: 0,
			value: 100,
			step: 10,
			slide: function( event, ui ) {
				$("#voval").html(ui.value);
				game.music.volume.setM(ui.value/100);
			},
			stop: function( event, ui ) {
				game.profile.saveOptions();
			}
		});
		
		$( "#vol-slider2" ).slider({
			orientation: "horizontal",
			range: "min",
			max: 100,
			min: 0,
			value: 50,
			step: 10,
			slide: function( event, ui ) {
				$("#voval2").html(ui.value);
				game.music.volume.setS(ui.value/100);
				game.music.p("blip", true);
			},
			stop: function( event, ui ) {
				game.profile.saveOptions();
			}
		});
		
		var isDown = false;
		$(document).ready(function() {
			// executes when HTML-Document is loaded and DOM is ready
			//alert("document is ready");

			$(document).mousedown(function() {
				isDown = true;      // When mouse goes down, set isDown to true
			})
			.mouseup(function() {
				isDown = false;    // When mouse goes up, set isDown to false
			});
			
			
			
		});
		
		$(window).load(function() {
			// executes when complete page is fully loaded, including all frames, objects and images
			//alert("doc is loaded");
			switchToMainMenu();
			game.init();
		});
		
		$(document).keypress(function(e) {
		if (game.inMainMenu==false) { 
			if(e.charCode==96) { game.togglePause(); } 
			if(e.keyCode==27) {
				if ($("#pause-menu").dialog("isOpen")==false) {
					$("pause-menu").dialog( "open" );
				} else { 
				$("#pause-menu").dialog( "close" ); }}
		}} );
		$("a").mouseenter(function(event) { if(isDown==false) { game.music.p("blip", true); } });
		$("button").mouseenter(function(event) { if(isDown==false) { game.music.p("blip", true); } });
		$(".game-dialog").bind("dialogclose", function() { game.music.p("blip down", true); });
		$(".game-dialog").bind("dialogopen", function(event) { game.music.p("blip up", true); });
		
		$("#prev, #pause, #next").on("mousedown", function(event) { game.music.p("blip", true); game.ui.updateSoundPlayer(event.target.id); game.music.p(event.target.id, false); });
	})
	;