var game = {};

game.version = "0.0.5.20.1"; //obviously it's the game-version identifier. Format: Major.minor.sub-minor.revision.pkg-alpha/beta/dev/release(stable)

console.print = function(){
	var string = "";
	for (var i = 0; i < arguments.length; i++) {
		string += arguments[i] + " ";
	}
	$(".console").html($(".console").html()+"<br/>"+string);
	$(".console").scrollTop($(".console")[0].scrollHeight);
	//console.log(string)
}

logStats = function(){
	var string = "";
	for (var i = 0; i < arguments.length; i++) {
		string += arguments[i] + " ";
	}
	$("#stats").html($("#stats").html()+"<br/>"+string);
	//$("#stats").scrollTop($("$stats")[0].scrollHeight);
}

logStatsB = function(){
	var string = "";
	for (var i = 0; i < arguments.length; i++) {
		string += arguments[i] + " ";
	}
	$("#stats").html($("#stats").html()+string);
	//$("#stats").scrollTop($("$stats")[0].scrollHeight);
}

logStatsClr = function(){
	$("#stats").html("<br>");
	$("#stats").scrollTop($("#stats")[0].scrollHeight);
}

console.clear = function(){
	$(".console").html("<br>");
	$(".console").scrollTop($(".console")[0].scrollHeight);
}

Array.prototype.removeItems = function(item, array) {
	var t = [];
	var count = 0;
	for(i=0;i<this.length;i++) { if(this[i]!=item) { this[count] = this[i]; count++; } }
	this.length = count;
}

Array.prototype.removeIndex = function(index) {
	var t = [];
	var count = 0;
	for(i=0;i<this.length;i++) { if(i==index) { count++; } else { t[i-count]=this[i]; } }
	for(i=0;i<t.length;i++) { this[i] = t[i]; }
}

Array.prototype.addItem = function (item, index){
	var t = [];
	var count = 0;
	for(i=0;i<=this.length;i++) { if(i==index) { t[i]=item; count++; } else { t[i]=this[i-count]; } }
	for(i=0;i<t.length;i++) { this[i] = t[i]; }
}

Array.prototype.except = function (array1, array2){
	jQuery.grep(array1, function(el){ return jQuery.inArray(el, array2) == -1; });
	return array1;
}

game.math = {
	variableSum : function(arr){return arr[0]+arr[1]+arr[2];}
}

game.ssGet = function(key){return sessionStorage.getItem(key);};
game.lsGet = function(key){return localStorage.getItem(key);};
game.ssSet = function(key, data){sessionStorage.setItem(key, data);};
game.lsSet = function(key, data){localStorage.setItem(key, data);};

game.date = {
	year : 1960,
	month : 0,
	months : ["Jan.", "Feb.", "Mar.", "Apr.", "May.", "Jun.", "Jul.", "Aug.", "Sep.", "Oct.", "Nov.", "Dec."],
	week : 0,
	subweek : 0,
	subweeks : [".", "..", "...", "...."],
	cycle : 0, //smallest unit of time
	getYear : function(){ return this.year; },
	getMonth : function(){ return this.months[this.month]; },
	getMonthNumber : function(){ return this.month+1; },
	getWeek : function(){ return this.week+1; },
	getSubweek : function(){ return this.subweek+1; },
	getCycle : function(){ return this.cycle; },
	toString : function(){ return this.year+" "+this.months[this.month]+" Week: "+(this.week+1)+" "+this.subweeks[this.subweek]; },
	getNFD : function(){ return this.year+"/"+this.month+"/"+this.week+"/"+this.subweek; },
	setYear : function(year){ this.year = year; },
	setMonth : function(month){ this.month = month-1; },
	setWeek : function(week){ this.week = week-1; },
	setSubweek : function(subweek){ this.subweek = subweek-1; },
	setCycle : function(cycle){ this.cycle = cycle; },
	
	increment : 
		function(){
			if (this.cycle<(100/game.timeMultiplier)){
				this.cycle+=1;
			} else {
				this.cycle=0;
				if (this.subweek >= 3) { this.subweek = 0; 
					if (this.week >= 3) { this.week = 0;
						if (this.month >= 11) { 
							this.month = 0; this.year += 1; 
						} else {this.month+=1;}
					} else {this.week+=1;}
				} else {this.subweek+=1;}
			}
		},
		
	hasChanged : function(){ return this.cycle==1; },
	
	getWeekDecimal : function(){
		return this.year*48 + this.month*4 + this.week + this.subweek/4 + this.cycle/400; //4 subweeks of 100 cycles each, 100 cycles = 1 sw
	}
};

game.deltaTime = 16.7;
game.lastUpdate = Date.now();
game.state = 1;
game.debug = 0;
game.fps = []; game.fps.length=4;
game.fpsTimer = Date.now();
game.fpsLabels = function(){ar = []; for (i=0;i<10;i++) { ar[i]=(i/10).toString() } return ar;};
game.fps.data = { 
				labels : game.fpsLabels(),
				datasets : [ { 
					fillColor : "rgba(220,220,220,0.5)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					data : game.fps
				} ]
			}

game.debugStats = function(a){
	if(a==1) {
		game.fps.push(smoothedValue(game.deltaTime));
		while (game.fps.length>10) { game.fps.shift() }
		game.carcalc.dbg();
		if (Date.now() - game.fpsTimer>10) {
			game.fpsTimer = Date.now();
			game.fps.data.datasets[0].data = game.fps;
			fpsChart.Line(game.fps.data);
		}
	}
}

var smoothed = 0;
var smoothing = 2;
function smoothedValue( newValue ){ 
	var elapsedTime = Date.now() - game.lastUpdate;
	smoothed += 3*(Math.random()-0.5);
	smoothed += elapsedTime * ( newValue - smoothed ) / smoothing;
	//if(smoothed>100) { smoothed=0; }
	return smoothed.toFixed(2); 
}

game.difficulty = 2;

game.toggledbg = function(){
	if (game.debug==1) {game.debug=0;} else {game.debug=1;}
}
game.timeMultiplier = 1;
game.timer = 0;

game.setState = function(s){
	game.state = s;
}
game.togglePause = function(){
	if(game.state==1){
		game.state = 2;
	}else{
		game.state = 1;
	}
}

game.update = function() {
	if (game.state==0) { 
		window.close();
	} else if (game.state==1) {
		game.date.increment();
		
		$("#date").html(game.date.toString());
		$("#compStats").html("Money: $"+game.company.getMoney()+"<br>Player name: "+game.company.playerName+"<br>Company name: "+game.company.getName()+"<br>level: "+game.company.getLevel()+"<br>Difficulty: "+game.profile.difficulty+"<br>Experience: "+game.company.experience.getStat("company")+"/"+game.company.experience.getLevel("company")+"<br>RP: "+game.company.researchPoints);
		//update everything here
		//update(game.deltaTime*game.timeMultiplier);
		if(game.date.hasChanged()){//date has changed, so notifications should be checked, not sure about anything else
			game.notifications.checkNotifications(); //idealy it should be game.timeCycle==0 but then it doesnt run the first, first message.
		}

		game.tasks.checkCompletedTasks();
		
		
	} else {
		
	}
	var now = Date.now();
		game.deltaTime = now - game.lastUpdate;
		game.timer+=game.deltaTime;
		game.lastUpdate = now;
	game.music.calculate();
	game.debugStats(game.debug);
	setTimeout(game.update, (game.deltaTime>16.66666)?0:(16.66666-game.deltaTime));
}

game.saveSlot = 0;
game.init = function() {
	$("#cSel").val("sports");
	$("#tSel").val("hatchback");
	$("#sSel").val("1");
	fpsChart = new Chart($("#fpsChart")[0].getContext("2d"));
	game.ui.updateSaveSlots();
	game.music.init();
	game.profile.loadOptions();
	setTimeout(game.update(), 10);
}

game.start = function() {
	game.notifications.init();
	game.company.init();
	
	$("#date").html(game.date.toString());
}

game.newProfile = function() {
	game.company.playerName = $("#pnBox").val();
	game.company.name = $("#cnBox").val();
	game.company.money = parseInt($("#moneyBox").val());
	game.company.level = parseInt($("#lvlBox").val());
	game.profile.difficulty = parseInt($('#radio input[name=difficulty]:checked').val());
	game.date.year = parseInt($("#yrBox").val());
	game.date.month = parseInt($("#moBox").val());
}

game.inMainMenu = true;

game.getStats = function(){
	var score = game.carcalc.dbg();
	return "Score: "+score+"\nCategory: "+car.category+"\nStyle: "+car.style+"\Size: "+car.size+"\nPerformance: "+car.variables[0]+"\nStyling: "+car.variables[1]+"\nComfort "+car.variables[2];
}

game.experienceLevels = [0,1e3,25e2,5e3,1e4,25e3,5e4,1e5,25e4,5e5,1e6,25e5,5e6,1e7]; //1e7 = 10,000,000, pattern is: 10,25,50,100,250,500,1000...