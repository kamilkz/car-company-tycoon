game.carstyles = {
	styles : [ {
		id : "hatchback",
		name : "Hatchback",
		typeMultiplier : [4,5,6,3,2,1,0,3],
		sizeMultiplier : [6,3,1],
		baseCost : 6e3,
		targetPrice : 25e3
	}, {
		id : "sedan",
		name : "Sedan",
		typeMultiplier : [4,3,3,5,6,3,0,3],
		sizeMultiplier : [3,5,5],
		baseCost : 6e3,
		targetPrice : 29e3
	}, {
		id : "wagon",
		name : "Station Wagon",
		typeMultiplier : [2,1,3,6,4,1,0,6],
		sizeMultiplier : [2,5,5],
		baseCost : 6e3,
		targetPrice : 31e3
	}, {
		id : "coupe",
		name : "Coupe",
		typeMultiplier : [6,3,2,0,3,6,0,0],
		sizeMultiplier : [4,5,2],
		baseCost : 7e3,
		targetPrice : 4e4
	}, {
		id : "suv",
		name : "SUV",
		typeMultiplier : [3,0,0,4,5,1,5,4],
		sizeMultiplier : [2,4,6],
		baseCost : 9e3,
		targetPrice : 42e3
	}, {
		id : "pickup",
		name : "Pickup",
		typeMultiplier : [2,0,1,2,1,0,6,6],
		sizeMultiplier : [2,4,6],
		baseCost : 7e3,
		targetPrice : 2e4
	}, {
		id : "crossover",
		name : "Crossover",
		typeMultiplier : [3,5,5,4,3,0,3,3],
		sizeMultiplier : [5,5,3],
		baseCost : 8e3,
		targetPrice : 35e3
	}, {
		id : "van",
		name : "Van",
		typeMultiplier : [0,0,3,2,1,0,2,6],
		sizeMultiplier : [2,4,6],
		baseCost : 7e3,
		targetPrice : 2e4
	}, {
		id : "convertible",
		name : "Convertible",
		typeMultiplier : [5,5,2,0,4,4,0,0],
		sizeMultiplier : [5,5,3],
		baseCost : 1e4,
		targetPrice : 45e3
	}, {
		id : "gt",
		name : "Grand Tourer",
		typeMultiplier : [6,2,2,0,5,6,0,0],
		sizeMultiplier : [5,6,3],
		baseCost : 14e3,
		targetPrice : 9e4
	}, {
		id : "limousine",
		name : "Limousine",
		typeMultiplier : [0,0,1,3,6,1,0,1],
		sizeMultiplier : [0,4,6],
		baseCost : 12e3,
		targetPrice : 6e4
	}, {
		id : "muscle",
		name : "Muscle car",
		typeMultiplier : [6,3,1,1,3,5,0,0],
		sizeMultiplier : [3,4,5],
		baseCost : 5e3,
		targetPrice : 25e3
	}, {
		id : "minivan",
		name : "Minivan",
		typeMultiplier : [0,0,2,6,5,0,1,5],
		sizeMultiplier : [0,3,6],
		baseCost : 8e3,
		targetPrice : 35e3
	} ],
	get : function(name) { for (i=0;i<game.carstyles.styles.length;i++) {if (this.styles[i].id==name){return this.styles[i];}}}
};