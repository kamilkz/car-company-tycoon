game.notifications = {};

(function(){
	var e = game.notifications;
	
	e.notifyDates = [];
	
	e.opened = false;
	e.dialogClosed = function(){
		this.opened = false;
	};
	e.push = function(notification){
		if(this.opened==false){
			$("#gdmHeader").html(notification.header);
			$("#gdmContent").html(notification.text);
			$("#game-dialog-message").dialog("open");
			this.opened = true;
		}
	};
	e.init = function(){
		for (i=0;i<game.notifications.media.length;i++) { 
			this.notifyDates[i] = game.notifications.media[i].date; 
		}
	};
	e.checkNotifications = function(){
		var i = this.notifyDates.indexOf(game.date.getNFD());
		if (i>=0) { game.notifications.push(game.notifications.media[i]); }
	};
	
	e.researchCompleted = function( research ) {
		this.push({header:"Research Completed", text: "Completed research for:<br/>"+research.name});
	};
	
	e.carCompleted = function( car ) {
		this.push({header:"Car Completed", text: car.name+" is now ready to be sold."});
	};
})();

game.notifications.media = [
	{
		id : "tutorial-start",
		date : "1960/0/0/0",
		header : "Tutorial",
		text : "Hello, welcome to Car Company Tycoon, a game where you own a car company and thrive for sucess in the car industry. We hope you enjoy your game, let's start by naming your company!",
		image : "lilwayne.mp3.png"
	},
	{
		id : "tutorial-start",
		date : "1960/0/1/0",
		header : "wk2",
		text : "week2",
		image : "lilwayne.mp3.png"
	},
	{
		id : "tutorial-start",
		date : "1960/0/2/0",
		header : "wk3",
		text : "week3",
		image : "lilwayne.mp3.png"
	},
	{
		id : "tutorial-start",
		date : "1960/0/3/0",
		header : "wk4",
		text : "week4",
		image : "lilwayne.mp3.png"
	}
];