game.ui = {};

/**
Most of the things are in main.js for things like buttons, 
but here is a file for directly interfacing the game and/or ui for things which are more than 2 lines.
*/

(function(){
	var e = game.ui;

	e.devCar = 0;
	e.curResearch = 0;
	
	e.updateSelection = function( element ){
		switch(element.id){
			case "cSel":
				this.devCar.category = element.value;
				break;
			case "tSel":
				this.devCar.style = element.value;
				break;
			case "sSel":
				this.devCar.size = element.value;
				break;
		}
	};
	e.name = function ( element ) {
		this.devCar.name = element.value;
	}
	
	e.handleSlider = function(sliderid,val){
		switch(sliderid){
			case "a-slider":
				$("#a-val").html(val);
				this.devCar.variables[0] = val;
				break;
			case "b-slider":
				$("#b-val").html(val);
				this.devCar.variables[1] = val;
				break;
			case "c-slider":
				$("#c-val").html(val);
				this.devCar.variables[2] = val;
				break;
			default:
				break;
		}
	};
	
	e.handleDateSlider = function(slider){
		game.timeMultiplier = slider.value;
	};
	
	e.createCar = function(){
		game.setState(2); 
		this.devCar = game.tasks.createNewCar();
		$( "#design-car" ).dialog( "open" );
	};

	e.displayCarDevDialog = function(car, stage){
		if(stage==0){//design phase
			$( "#design-car" ).dialog( "open" );
			$( "#design-tabs" ).tabs( "option", "active", 1 );
		}else if(stage==1){//prototype
			$( "#design-car" ).dialog( "open" );
			$( "#design-tabs" ).tabs( "option", "active", 2 );
		}else if(stage==2){//manufacturing
			$( "#design-car" ).dialog( "open" );
			$( "#design-tabs" ).tabs( "option", "active", 3 );
		}else if(stage==null){//overview
			$( "#design-car" ).dialog( "open" );
			$( "#design-tabs" ).tabs( "option", "active", 0 );
		}
	}
	
	e.updateProgressBar = function(type, progress, var2){
		var value = 0;
		var text = 0;
		if(type=="car"){
			if(game.tasks.developingCar){
				value = (progress*100);
				text = "Stage "+var2;
			}else{
				value = 0;
				text = "No development";
			}
		}else if(type=="research"){
			if(game.tasks.isResearching){
				value = progress*100;
				text = "Researching ("+value.toFixed()+"%)";
			}else{
				value = 0;
				text = "No Research";
			}
		}else if(type=="load"){value = progress; text=var2;}
		$("#"+type+"-progress-label").text(text);
		$("#"+type+"-progress").progressbar("value", value);
	};
	
	e.updateSaveSlot = function(slot){
		if(game.profile.exists(slot)===false) { 
			$("#sv"+slot).html("--no savedata--") 
		} else { 
			var meta = game.profile.getMeta(slot);
			$("#sv"+slot).html("company: "+meta.companyName+" -- $"+meta.money+"<br>"+meta.date.year+"/"+meta.date.month+"/"+meta.date.week); 
		}
	};
	
	e.updateSaveSlots = function(){
		this.updateSaveSlot(1);
		this.updateSaveSlot(2);
		this.updateSaveSlot(3);
	};	
	
	e.updateSoundPlayer = function(id) {
		if (id=="pause"||id=="ps") {
			game.music.playing ? $("#ps").css('background', "url('./themes/cct/images/playback-ui2.png') -78px -26px") : $("#ps").css('background', "url('./themes/cct/images/playback-ui2.png') 0 -26px")
		}
	};
	
	e.updateResearchSelection = function(researchid) {
		this.curResearch = researchid.value;
	};
	
	e.startResearch = function(){
		//if(isOptionSelected){
			var research = game.research.getItemByID(this.curResearch);
			if(research != undefined){
				if(game.research.canResearch(research)){
					game.tasks.startResearch(research);
					return true;
				}
			}
		//}
		return false;
	};
	
	e.populateResearches = function() {
		var researchesHtml = "<select id='rSel' onchange='game.ui.updateResearchSelection(this)'>";
		
		var r = game.company.researchAvailable;
		
		for (var i = 0; i < r.length; i++) {
			researchesHtml += "<option value='" + r[i].id + "'>" + r[i].name + " (" + r[i].pointsCost + "RP" + ( r[i].cost != undefined ? ( " " + r[i].cost + "K)</option>" ) : ( ")</option>" ) );
		}
		
		researchesHtml += "</select>";
		
		$("#researches").html(researchesHtml);
	};
	
})();