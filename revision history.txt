v. 0.0.4.4.0 - 23/Dec/2013 09:15 - simple ui stuff, and a bit more js --lantaren
v. 0.0.4.3.0 - 23/Dec/2013 07:25 - UI work with jQuery --KamilKZ
v. 0.0.4.2.0 - 23/Dec/2013 05:46 - changed simple ui stuff --lantaren
v. 0.0.4.1.0 - 23/Dec/2013 01:30 - log added (changed gameVersion to an actual string) --lantaren
v. 0.0.4.0.0 - 23/Dec/2013 01:24 - gameversion string, fixed some UI, created a % off of correct ratio thing, pause when notification popup --lantaren
v. 0.0.3.0.0 - 22/Dec/2013 17:29 - more jqueryUI stuff implemented --KamilKZ
v. 0.0.2.1.3 - 22/Dec/2013 14:39 - started the company object --KamilKZ
v. 0.0.2.1.2 - 22/Dec/2013 11:50 - added carstyles and started researches (not used at this point) --KamilKZ
v. 0.0.2.1.1 - 22/Dec/2013 11:31 - cct car types multipliers.xslx added --KamilKZ
v. 0.0.2.1.0 - 22/Dec/2013 01:39 - fixed spellng of 'luxury' --lantaren
v. 0.0.2.0.0 - 22/Dec/2013 01:36 - implemented cat., type, and size selection, from array carVar --lantaren
v. 0.0.1.4.0 - 22/Dec/2013 00:49 - notifications, gamestates, month tracking, player stats --lantaren
v. 0.0.1.0.0 - 21/Dec/2013 22:30 - simple game loop implemented w/ speed multiplier --lantaren
v. 0.0.0.2.0 - 21/Dec/2013 16:41 - jqueryUI stuff --KamilKZ
v. 0.0.0.1.2 - 21/Dec/2013 16:35 - node webkit added --lantaren
v. 0.0.0.1.1 - 21/Dec/2013 16:19 - jquery added --KamilKZ
v. 0.0.0.1.0 - 21/Dec/2013 15:37 - changed title --lantaren
v. 0.0.0.0.0 - 21/Dec/2013 15:34 - project setup --KamilKZ